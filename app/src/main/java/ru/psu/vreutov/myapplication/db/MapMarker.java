package ru.psu.vreutov.myapplication.db;

import com.google.android.gms.maps.model.LatLng;

import io.realm.RealmObject;

/**
 * Created by VReutov on 03.04.2018.
 */

public class MapMarker extends RealmObject {
    private double lattitue;
    private double longitude;

    public LatLng getPosition(){
        return new LatLng(lattitue, longitude);
    }

    public void setPosition(LatLng value) {
        lattitue = value.latitude;
        longitude = value.longitude;
    }
}
